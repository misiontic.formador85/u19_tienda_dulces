const express = require("express")
const fileUpload = require("express-fileupload")
const cors = require("cors")
const routerUsuario = require("../routes/usuario")
const routerProducto = require("../routes/producto")
const conexionDB = require("./database")
const dotenv = require("dotenv").config()
const path = require("path")

class Server {
  constructor() {
    this.port = process.env.PORT
    this.app = express()
    this.middlewares()
    this.app.listen(this.port, () => {
      console.log("se esta ejecutando la app")
    })
    this.routes()

    conexionDB()
  }

  middlewares() {
    // MIDDLEWARES
    this.app.use(express.json())
    this.app.use(fileUpload({
      useTempFiles : true,
      tempFileDir : '/tmp/'
    }))
    this.app.use( cors() )
    this.app.use(express.static('public'))
  }

  routes() {
    this.app.use( "/usuario", routerUsuario )
    this.app.use( "/producto", routerProducto )
    this.app.use( "/auth", require("../routes/auth") )
    this.app.get("*", (req, res) => {
      res.sendFile(path.join(__dirname,"../../public/index.html"))
    })

  }
}

module.exports = Server
