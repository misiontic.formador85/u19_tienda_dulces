const mongoose = require("mongoose")
const dotenv = require("dotenv").config()

// 1. Conectarse

const conexionDB = () => {
  mongoose
    .connect(process.env.URL_DATABASE_PROD || process.env.URL_DATABASE_DEV)
    .then(() => {
      console.log("connection successful")
    })
    .catch(() => {
      console.log("no connection")
    })
}

module.exports = conexionDB
