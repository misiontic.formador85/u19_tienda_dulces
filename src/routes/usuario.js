const { Router } = require("express")
const {
  crearUsuario,
  getUsuarios,
  getUsuario,
  modificarUsuario,
  borrarUsuario,
} = require("../controllers/usuario")
const validarToken = require("../middlewares/auth")

const routerUsuario = Router()

// ESTA ES LA RUTA PARA CREAR UN USUARIO
routerUsuario.post("", crearUsuario)
routerUsuario.get("", [validarToken], getUsuarios)
routerUsuario.get("/:id", [validarToken], getUsuario)
routerUsuario.put("", [validarToken], modificarUsuario)
routerUsuario.delete("", [validarToken], borrarUsuario)

module.exports = routerUsuario
