const {Schema, model} = require("mongoose")

// 1. Crear esquema
const usuarioSchema = new Schema( {
  tipoDocumento: String,
  numDocumento: String,
  nombre: String,
  apellido: String,
  email: String,
  telefonoContacto: String,
  password: String,
  tipo: {
    type: String,
    enum: ["CLIENTE", "ADMIN", "VENDEDOR"]
  }
} )

// 2. Crear el modelo
const UsuarioModel = model( "usuario" , usuarioSchema )

module.exports = UsuarioModel