const { request, response } = require("express")
const {hashSync, genSaltSync, compareSync} = require("bcryptjs")
const UsuarioModel = require("../models/usuario")


// CREAR
async function crearUsuario(req = request, res = response) {
  const { numDocumento, password } = req.body

  const usuarioEncontrado = await UsuarioModel.findOne({
    numDocumento
  })

  // VALIDACION
  if (usuarioEncontrado) {
    res.status(400).send({ mensaje: "El usuario ya existe" })
  } else {

    const passwordEncrypted = hashSync(password, genSaltSync())
    
    req.body.password = passwordEncrypted

    UsuarioModel.create(req.body)
      .then((usuarioCreado) => {
        res.status(201).send({ mensaje: "Se creo el usuario", usuarioCreado })
      })
      .catch(() => {
        res.send({ mensaje: "No se logro crear el usuario" })
      })
  }
}

async function getUsuarios(req = request, res = response) {
  const { id, email, numDocumento } = req.query

  if(id || email || numDocumento ){
    const usuario = await UsuarioModel.findOne({$or: [{_id: id},{email},{numDocumento}] })
    return res.send(usuario)
  } else {
    const listausuarios = await UsuarioModel.find()
    res.send(listausuarios)
  }

}

async function getUsuario(req = request, res = response) {
  const { id } = req.params

  const usuario = await UsuarioModel.find({ email: id})

  res.send(usuario)
}

async function modificarUsuario(req = request, res = response) {

  const {id, password, ...usuario} = req.body

  const passwordEncrypted = hashSync(password, genSaltSync())
  usuario.password = passwordEncrypted

  await UsuarioModel.updateOne({_id: id}, usuario )
  const usuarioModificado = await UsuarioModel.findById(id)

  res.send(usuarioModificado)

}

async function borrarUsuario(req = request, res = response) {
  
  const {id} = req.body

  const object = await UsuarioModel.findByIdAndDelete(id)

  res.send(object)

}


module.exports = { crearUsuario, getUsuarios, getUsuario, modificarUsuario, borrarUsuario }
