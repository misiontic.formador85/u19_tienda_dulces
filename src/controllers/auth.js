const { request, response } = require("express")
const { compareSync } = require("bcryptjs")
const { sign } = require("jsonwebtoken")
const UsuarioModel = require("../models/usuario")

async function login(req = request, res = response) {
  const { email, password } = req.body

  // 1. Buscar usuario
  try {
    const usuarioBD = await UsuarioModel.findOne({ email })
    if (usuarioBD) {
      // SI LAS CREDENCIALES SON VALIDAS
      if (compareSync(password, usuarioBD.password)) {
        // CREAMOS Y ENVIAMOS EL TOKEN
        sign(
          {
            id: usuarioBD.id,
            tipo: usuarioBD.tipo,
            nombre: usuarioBD.nombre,
            apellidos: usuarioBD.apellido,
          },
          "grupo20-tiend4$Dulces",
          { expiresIn: "5h" },
          (err, token) => {
            if (err) res.status(500).send({ mensaje: "hubo un error" })
            else res.send({ auth: true, token })
          }
        )
      }
    } else res.status(400).send({ mensaje: "no existe el usuario" })
  } catch (e) {
    res.status(500).send({ mensaje: "error interno del servidor" })
  }
}

function validarJWT(req, res) {
  res.status(200).send({ auth: true })
}

module.exports = { login, validarJWT }
