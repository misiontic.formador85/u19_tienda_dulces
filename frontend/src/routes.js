import React from "react"
import Home from "./pages/home"
import Login from "./pages/login"
import { Routes, Route } from "react-router-dom"
import PrivateRoute from "./config/auth"
import Admin from "./pages/admin"
import UpdateUsuario from "./pages/admin/update-usuario"
import Usuarios from "./pages/admin/usuarios"
import Register from "./pages/register"
import UpdateProducto from "./pages/admin/update-producto"

export default function RoutesApp({ isAuth, setIsAuth }) {
  return (
    <Routes>
      <Route index element={<Home />} />
      <Route
        path="/admin"
        element={
          <PrivateRoute isAuth={isAuth} >
            <Admin />
          </PrivateRoute>
        }>
        <Route path="usuario/all" element={<Usuarios />} />
        <Route path="usuario/:id" element={<UpdateUsuario />} />
        <Route path="producto/:id" element={<UpdateProducto />} />
      </Route>
      <Route path="/login" element={<Login titulo={"Mi tienda de dulces"} setIsAuth={setIsAuth} />} />
      <Route path="/register" element={<Register />} />
    </Routes>
  )
}
