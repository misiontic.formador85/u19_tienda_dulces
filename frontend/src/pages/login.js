import axios from "axios"
import React from "react"
import { useNavigate } from "react-router-dom"
import { BASE_URL } from "../config/constants"

export default function Login( {titulo, setIsAuth} ) {

  const navigate = useNavigate()


  async function formulario( event ) {

    event.preventDefault() // Evita que HTML reenvie una petición

    const {password, email} = event.target

    try {
      const res = await axios.post( BASE_URL + "/auth/login", { email: email.value, 
      password: password.value })
      localStorage.setItem("token", res.data.token)
      setIsAuth(true)
      navigate("/admin")
    } catch (error) {
      alert("Credenciales incorrectas")
    }

  }



  return (
    <div className="login-page">
      <div className="login-box">
        <div className="login-logo">
          <a href="#">
            <b>{titulo}</b>
          </a>
        </div>
        <div className="card">
          <div className="card-body login-card-body">
            <p className="login-box-msg">Inicia sesión</p>
            <form onSubmit={ formulario }>
              <div className="input-group mb-3">
                <input
                  type="email"
                  className="form-control"
                  name="email"
                  placeholder="Correo electrónico"
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope" />
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input
                  type="password"
                  name="password"
                  className="form-control"
                  placeholder="Contraseña"
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-8">
                  <div className="icheck-primary">
                    <input type="checkbox" id="remember" />
                    <label htmlFor="remember">Recuérdame</label>
                  </div>
                </div>
                <div className="col-4">
                  <button type="submit" className="btn btn-info btn-block">
                    Inicia sesión
                  </button>
                </div>
              </div>
            </form>
            <p className="mb-1">
              <a href="forgot-password.html">Olvide mi contraseña</a>
            </p>
            <p className="mb-0">
              <a href="register.html" className="text-center">
                ¿No tienes cuenta? Registrate
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}
