import imagen from "../assets/logo192.png"

import "./sidebar.css"

import { FaUserFriends, FaUserPlus } from "react-icons/fa"

import React, { useState } from "react"
import { Link, NavLink } from "react-router-dom"
import { payload, getPayload } from "../config/constants"

export default function Sidebar() {
  const [itemUsuarioOpen, setItemUsuarioOpen] = useState(true)
  //let itemUsuarioOpen = true

  return (
    <aside className="main-sidebar sidebar-dark-primary elevation-4 altura">
      <Link to="/" className="brand-link">
        <i className="fa-thin fa-candy"></i>
        <span className="brand-text font-weight-light">Tienda Dulces</span>
      </Link>
      <div>
        <div className="sidebar">
          <div className="user-panel mt-3 pb-3 mb-3 d-flex">
            <div className="image">
              <img src={imagen} className="img-circle elevation-2" alt="User" />
            </div>
            <div className="info">
              <Link>
                {getPayload()?.nombre} {getPayload()?.apellidos}
              </Link>
            </div>
          </div>
        </div>
        <nav>
          <ul
            className="nav nav-pills nav-sidebar flex-column"
            data-widget="treeview"
            role="menu"
            data-accordion="false">
            {getPayload()?.tipo === "ADMIN" && (
              <li
                className={"nav-item" + (itemUsuarioOpen ? " menu-open" : "")}>
                <div
                  className="nav-link"
                  onClick={() => {
                    setItemUsuarioOpen(!itemUsuarioOpen)
                  }}>
                  <FaUserFriends size={24} className="nav-icon" />
                  <p>
                    Usuarios
                    <i className="right fas fa-angle-left" />
                  </p>
                </div>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <NavLink to="/admin/usuario/all" className="nav-link" end>
                      <i className="far fa-circle nav-icon" />
                      <p>Listar</p>
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink to="/admin/usuario/new" className="nav-link" end>
                      <FaUserPlus size={20} className="nav-icon" />
                      <p>Crear o Modificar</p>
                    </NavLink>
                  </li>
                </ul>
              </li>
            )}
            <li className={"nav-item" + (itemUsuarioOpen ? " menu-open" : "")}>
              <div
                className="nav-link"
                onClick={() => {
                  setItemUsuarioOpen(!itemUsuarioOpen)
                }}>
                <FaUserFriends size={24} className="nav-icon" />
                <p>
                  Productos
                  <i className="right fas fa-angle-left" />
                </p>
              </div>
              <ul className="nav nav-treeview">
                <li className="nav-item">
                  <NavLink to="/admin/producto/all" className="nav-link" end>
                    <i className="far fa-circle nav-icon" />
                    <p>Listar</p>
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to="/admin/producto/new" className="nav-link" end>
                    <FaUserPlus size={20} className="nav-icon" />
                    <p>Crear o Modificar</p>
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
    </aside>
  )
}
