import jwtDecode from "jwt-decode"
import Swal from "sweetalert2"

export const BASE_URL = "http://localhost:3001"
//export const BASE_URL = "https://misiontic-u19-tienda-dulces.herokuapp.com"

export const ALERT = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})

export const payload = jwtDecode(localStorage.getItem("token") || "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c" )

export function getPayload() {
  
  if (localStorage.getItem("token"))
    return jwtDecode(localStorage.getItem("token"))
  else
    return null
}