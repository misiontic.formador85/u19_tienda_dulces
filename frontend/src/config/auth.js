import React from 'react'
import { Navigate } from 'react-router-dom'




export default function PrivateRoute( {children, isAuth} ) {

  console.log("Se renderizo o se monto el componente PrivateRoute")

  if(isAuth){
    return children
  } else return (
    <Navigate to={"/login"} />
  )
  
}
